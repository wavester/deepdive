![](https://gitlab.com/wavester/deepdive/-/raw/master/www/img/gitlogo.png?width=400)

**R.I.P. (14.07.2020 / 24.07.2021)**

All the configuration files used for the [deep dive] community.

For Port Royal related stuff, please refer to the [[port royal]](https://gitlab.com/wavester/portroyal) git repository.

[Click here](https://discord.gg/BsmzzU6cvM) if you wish to join the Deep Dive museum.

![](https://gitlab.com/wavester/deepdive/-/raw/master/www/img/timeline.png?width=700)
